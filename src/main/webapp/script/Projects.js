function addProject(){

    var selectedUsers = [];
    $.each(document.AllUsers, function(key1, val1){
        $.each($('#selectMembers').val(), function(key2, val2){
            if(val1.id == parseInt(val2) ){
                selectedUsers.push(val1);
            }
        });
    });
    debugger;
    return $.ajax({
        url: "/bugTracker/ProjectService",
        data: {
            projectName: $("#projectName").val(),
            description: $("#description").val(),
            selectedUsers: JSON.stringify(selectedUsers)
        },
        type: "POST",
        dataType : "json",
    });
}
var save = function(){
    addProject().done(function( json ) {
        alert('Project Is Successfully Added');
        loadTab('project/Project.html');
    }).fail(function( xhr, status, errorThrown ) {
        //TODO
        console.log(xhr+ " "+ status+ " "+ errorThrown );
    });
};

var selectEmployee = function(){
    userService().getAll().done(function( resp ) {
        if(resp.status == "success"){
            document.AllUsers = resp.data;
            $('#selectMembers').empty();
            $.each(resp.data, function(index, user){
                var opt = appendHtmlElement($('#selectMembers'),"<option></option>");
                opt.attr("value", user.id);
                opt.text(user.firstName+" "+ user.lastName);
            })
        }
    });
}

var getProjects = function(){
    searchInTable("#searchProject", "#displayAllProject");
        projectService().getAllProject().done(function( resp ) {
            if(resp.status == "success"){
                $('#displayAllProject').empty();
                var thead = appendHtmlElement($('#displayAllProject'), "<thead></thead>");
                var theadTr = appendHtmlElement(thead, "<tr></tr>");
                appendHtmlElement(theadTr, "<td></td>").text(" Project Name");
                appendHtmlElement(theadTr, "<td></td>").text("Description");
                appendHtmlElement(theadTr, "<td></td>").text("Created By");
                appendHtmlElement(theadTr, "<td></td>").text("Created Date");
                var tbody = appendHtmlElement($('#displayAllProject'), "<tbody></tbody>");
                $.each(resp.data, function(index, project){
                    var tr = appendHtmlElement(tbody, "<tr></tr>");
                    appendHtmlElement(tr, "<td></td>").text(project.name);
                    appendHtmlElement(tr, "<td></td>").text(project.description);
                    appendHtmlElement(tr, "<td></td>").text(project.createdBy);
                    appendHtmlElement(tr, "<td></td>").text(project.createdDate);

                });
            }
        }).fail(function( xhr, status, errorThrown ) {
            //TODO
            console.log(xhr+ " "+ status+ " "+ errorThrown );
        })
}

$(document).ready(function(){
    getProjects();
    selectEmployee();
});

/*
function appendHtmlElement(element, createDomStr){
    var dom = $(createDomStr);
    element.append(dom)
    return dom;
}*/
