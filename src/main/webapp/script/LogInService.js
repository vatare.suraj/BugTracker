function LogInService(){
    this.logIn = function(){
        return $.ajax({
            url: "../LoginService",
            data: {
                email: $("#userName").val(),
                password: $("#password").val()
            },
            type: "POST",
            dataType : "json",
        });
    }

    this.getCurrentUser = function(){
        return $.ajax({
            url: "../LoginService",
            type: "GET",
            dataType : "json",
        });
    }

    this.logOut = function(){
        return $.ajax({
            url: "../LoginService",
            type: "DELETE",
            dataType : "json",
        });
    }
    return this;
};