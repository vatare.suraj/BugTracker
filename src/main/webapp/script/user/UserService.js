
function userService(){

    this.getAll = function(){
        return $.ajax({
            url: "/bugTracker/UserService",
            type: "GET",
            dataType : "json",
        });
    }

    this.get = function(user){
        return $.ajax({
            url: "/bugTracker/UserService",
            data: {
                user: user
            },
            type: "GET",
            dataType : "json",
        });
    }
    return this;
}
