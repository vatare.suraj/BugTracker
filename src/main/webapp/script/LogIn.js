$(function(){
    $("#logIn").click(function(){
        LogInService().logIn().done(function( resp ) {
            if(resp.data.id == 0){
                $('#errorBox > .alert').text("Incorrect UserId or Password").removeClass('alert-success d-none').addClass('alert-danger');
            } else{
                $('#errorBox > .alert').text("Successfully LogedIn").removeClass('alert-danger d-none').addClass('alert-success');
                setTimeout(function(){ window.location.href="Home.html"; }, 6500);
            }
        }).fail(function( xhr, status, errorThrown ) {
            //TODO
            console.log(xhr+ " "+ status+ " "+ errorThrown );
        });
    });

});
