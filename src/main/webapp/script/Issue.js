function addReport(){
    /*var data = $('form').serialize();*/
    return $.ajax({
        url: "/bugTracker/ReportService",
        data: {
            issueName: $("#issueName").val(),
            project: $("#project").val(),
            category: $("#category").val(),
            priority: $("#priority").val(),
            status: $("#status").val(),
            assign: $("#assign").val(),
            description: $("#description").val(),
            reproduce: $("#reproduce").val()
        },
        type: "POST",
        dataType : "json",
    });
}
var reportSubmit = function(){
    addReport().done(function( json ) {
        alert('Report Is Successfully Added');
        loadTab('issue/ReportIssue.html')
    }).fail(function( xhr, status, errorThrown ) {
        //TODO
        console.log(xhr+ " "+ status+ " "+ errorThrown );
    });
};

var selectEmployee = function(){
    userService().getAll().done(function( resp ) {
        if(resp.status == "success"){
            document.AllUsers = resp.data;
            $('#selectEmp').empty();
            $.each(resp.data, function(index, user){
                var opt = appendHtmlElement($('#selectEmp'),"<option></option>");
                opt.attr("value", user.id);
                opt.text(user.firstName+" "+ user.lastName);
            })
        }
    });
}



var selectProject = function(){
    projectService().getAllProject().done(function( resp ) {
        if(resp.status == "success"){
            document.AllProject = resp.data;
            $('#selectProjects').empty();
            $.each(resp.data, function(index, project){
                var opt = appendHtmlElement($('#selectProjects'),"<option></option>");
                opt.attr("value", project.id);
                opt.text(project.name);
            })
        }
    });
}


var getAllIssue = function(){

    searchInTable("#searchIssue", "#displayIssue");
    reportService().getAllReport().done(function( resp ) {

        if(resp.status == "success"){
            $('#displayIssue').empty();
            var thead = appendHtmlElement($('#displayIssue'), "<thead></thead>");
            var theadTr = appendHtmlElement(thead, "<tr></tr>");
            appendHtmlElement(theadTr, "<td></td>").text("Name");
            appendHtmlElement(theadTr, "<td></td>").text("Project");
            appendHtmlElement(theadTr, "<td></td>").text("Category");
            appendHtmlElement(theadTr, "<td></td>").text("Description");
            appendHtmlElement(theadTr, "<td></td>").text("Status");
            appendHtmlElement(theadTr, "<td></td>").text("Priority");
            appendHtmlElement(theadTr, "<td></td>").text("Assigned User");
            appendHtmlElement(theadTr, "<td></td>").text("Created_Date");
            var tbody = appendHtmlElement($('#displayIssue'), "<tbody></tbody>");
            $.each(resp.data, function(index, report){
                var tr = appendHtmlElement(tbody, "<tr></tr>");
                appendHtmlElement(tr, "<td></td>").text(report.name);
                appendHtmlElement(tr, "<td></td>").text(report.project);
                appendHtmlElement(tr, "<td></td>").text(report.category);
                appendHtmlElement(tr, "<td></td>").text(report.description);
                appendHtmlElement(tr, "<td></td>").text(report.status);
                appendHtmlElement(tr, "<td></td>").text(report.priority);
                appendHtmlElement(tr, "<td></td>").text(report.assignTouser);
                appendHtmlElement(tr, "<td></td>").text(report.careatedDate);
            });
        }

    }).fail(function( xhr, status, errorThrown ) {
        //TODO
        console.log(xhr+ " "+ status+ " "+ errorThrown );
    })
}

$(document).ready(function(){
    getAllIssue();
    selectProject();
    selectEmployee();

});

/*

function appendHtmlElement(element, createDomStr){
    var dom = $(createDomStr);
    element.append(dom)
    return dom;
}
*/
