 // this JQuery function will execute on document load
$(document).ready(function(){
    // ondocument load
    $('#topMenu').load('TopMenu.html', function(){
        LogInService().getCurrentUser().done(function( resp ) {
            if(resp.data == undefined || resp.data.id == 0){
                window.location.href="Login.html";
            }else{
                document.currentUser = resp.data;
                $("#currentUserName").text(document.currentUser.firstName);
            }
        }).fail(function( xhr, status, errorThrown ) {
           //TODO
           console.log(xhr+ " "+ status+ " "+ errorThrown );
        });

        $("#logout").click(function(){
            LogInService().logOut().done(function( resp ) {
                document.currentUser = undefined;
                window.location.href="Login.html";
            }).fail(function( xhr, status, errorThrown ) {
                //TODO
                console.log(xhr+ " "+ status+ " "+ errorThrown );
            });
        });

    });
    $('#leftMenu').load('LeftMenu.html');

});

var loadTab = function(pageUrl){
    $('#displayEvent').load(pageUrl);
};

var searchInTable = function(inputId, tableId){
    $(inputId).on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $(tableId + " tr").filter(function() {
            $(this).toggle( $(this).text().toLowerCase().indexOf(value) > -1 )
        });
    });
}


var appendHtmlElement = function(element, createDomStr){
    var dom = $(createDomStr);
    element.append(dom)
    return dom;
}