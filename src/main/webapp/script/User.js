$(document).ready(function(){
    searchInTable("#searchUser", "#displayUsers");
    userService().getAll().done(function( resp ) {
        if(resp.status == "success"){
            $('#displayUsers').empty();
            var thead = appendHtmlElement($('#displayUsers'), "<thead></thead>");
            var theadTr = appendHtmlElement(thead, "<tr></tr>");
            appendHtmlElement(theadTr, "<td></td>").text("User Name");
            appendHtmlElement(theadTr, "<td></td>").text("Email");
            appendHtmlElement(theadTr, "<td></td>").text("Role");
            appendHtmlElement(theadTr, "<td></td>").text("Created Date");
            appendHtmlElement(theadTr, "<td></td>").text("Status");
            var tbody = appendHtmlElement($('#displayUsers'), "<tbody></tbody>");
            $.each(resp.data, function(index, user){
                var tr = appendHtmlElement(tbody, "<tr></tr>");
                appendHtmlElement(tr, "<td></td>").text(user.firstName+" "+ user.lastName);
                appendHtmlElement(tr, "<td></td>").text(user.email);
                appendHtmlElement(tr, "<td></td>").text(user.role);
                appendHtmlElement(tr, "<td></td>").text(user.created_date);
                appendHtmlElement(tr, "<td></td>").text(user.isActive);
            });
        }

    }).fail(function( xhr, status, errorThrown ) {
        //TODO
        console.log(xhr+ " "+ status+ " "+ errorThrown );
    })
});
/*

function appendHtmlElement(element, createDomStr){
    var dom = $(createDomStr);
    element.append(dom)
    return dom;
}
*/
