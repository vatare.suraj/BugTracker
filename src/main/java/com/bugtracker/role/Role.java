package com.bugtracker.role;

public enum Role {
    ADMIN("admin"), MANAGER("manager"), DEVELOPER("developer"), TESTER("TESTER");

    private String role;

    public String getRole() {
        return this.role;
    }

    private Role(String role){
        this.role = role;
    }
}
