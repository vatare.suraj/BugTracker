package com.bugtracker.report;
import com.bugtracker.common.GSONResponce;
import com.bugtracker.project.Project;
import com.bugtracker.security.ValidateSession;
import com.bugtracker.user.User;
import com.google.gson.Gson;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@WebServlet("/ReportService")
public class ReportService extends HttpServlet{
    ReportManager reportManager;
    Gson gson = new Gson();
    @Override
    public void init() throws ServletException {
        super.init();
        reportManager = new ReportManager();
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=UTF-8");
        try {
            ValidateSession.validate(req, resp);
            // user.setId(Integer.parseInt(req.getParameter("id")));
            List<Report> list = reportManager.getAllReport();
            resp.getWriter().print( gson.toJson(new GSONResponce("success", "", list)) );
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=UTF-8");
        System.out.println("Service is call");
            Report report = new Report();
        try{
            // check user authentication
            User user = ValidateSession.validate(req, resp);
            report.setName(req.getParameter("issueName"));
            report.setProject(req.getParameter("project"));
            report.setCategory(req.getParameter("category"));
            report.setPriority(req.getParameter("priority"));
            report.setStatus(req.getParameter("status"));
            report.setAssignTouser(req.getParameter("assign"));
            report.setDescription(req.getParameter("description"));
            report.setStepToReproduce(req.getParameter("reproduce"));
            report.setCareatedDate(new Date());
            System.out.println("IssueName"+report.getName());
            System.out.println("ProjectName"+report.getProject());
            reportManager.addReport(report);
            System.out.println("service End");
            resp.getWriter().print( gson.toJson(new GSONResponce("success", "", report)) );
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
