package com.bugtracker.report;

import java.util.List;

public class ReportManager {
    ReportDao reportDao;
    public ReportManager(){
        reportDao = new ReportDao();
    }
    public void addReport(Report report){
        reportDao.addReport(report);
    }
    public List<Report> getAllReport(){return reportDao.getAllReport();}
}
