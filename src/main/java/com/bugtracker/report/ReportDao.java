package com.bugtracker.report;

import com.bugtracker.common.DBConnection;
import com.bugtracker.project.Project;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class ReportDao {

    public void addReport(Report report){
        try {
            System.out.println("reportDao call");
            Connection con = DBConnection.getConnection();
            String query = " insert into issueReport (issueName, description, project, status , priority , assignToUser , steptoReproduce , category , created_date)"
                    + " values (?, ?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setString(1,report.getName());
            stmt.setString (2,report.getDescription());
            stmt.setString(3,report.getProject());
            stmt.setString(4,report.getStatus());
            stmt.setString(5,report.getPriority());
            stmt.setString(6,report.getAssignTouser());
            stmt.setString(7,report.getStepToReproduce());
            stmt.setString(8,report.getCategory());
           stmt.setTimestamp(9,new Timestamp(report.getCareatedDate().getTime()));
            int i = stmt.executeUpdate();
            if(i!=0)
                System.out.println(" Report is inserted successfully");
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
    public List<Report> getAllReport(){
        List<Report> list = new ArrayList<Report>();
        try {
            Connection con = DBConnection.getConnection();
            PreparedStatement stmt = con.prepareStatement(" select * from issueReport");
            ResultSet resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                Report report = new Report();
                report.setId(resultSet.getInt("reportID"));
                report.setName(resultSet.getString("issueName"));
                report.setDescription(resultSet.getString("description"));
                report.setStepToReproduce(resultSet.getString("steptoReproduce"));
                report.setProject(resultSet.getString("project"));
                report.setStatus(resultSet.getString("status"));
                report.setPriority(resultSet.getString("priority"));
                report.setAssignTouser(resultSet.getString("assignToUser"));
                report.setCategory(resultSet.getString("category"));
                report.setCareatedDate(resultSet.getDate("created_date"));
                list.add(report);
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return list;
    }
}
