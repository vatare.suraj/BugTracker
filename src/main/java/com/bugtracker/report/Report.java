package com.bugtracker.report;

import java.util.Date;

public class Report {
    private int id;
    private String name;
    private String description;
    private String stepToReproduce;
    private String project;
    private String status;
    private String priority;
    private String assignTouser;
    private String category;

    public Date getCareatedDate() {
        return careatedDate;
    }

    public void setCareatedDate(Date careatedDate) {
        this.careatedDate = careatedDate;
    }

    private Date careatedDate;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStepToReproduce() {
        return stepToReproduce;
    }

    public void setStepToReproduce(String stepToReproduce) {
        this.stepToReproduce = stepToReproduce;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getAssignTouser() {
        return assignTouser;
    }

    public void setAssignTouser(String assignTouser) {
        this.assignTouser = assignTouser;
    }
}
