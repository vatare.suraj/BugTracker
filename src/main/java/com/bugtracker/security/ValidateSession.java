package com.bugtracker.security;

import com.bugtracker.user.User;
import com.google.gson.Gson;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class ValidateSession {

    public static User validate(HttpServletRequest req, HttpServletResponse resp) throws IOException,InvalidUserException{
        HttpSession session = req.getSession(false);
        User user = null;
        if(session == null || session.isNew()){
            ErrorMessage error = new ErrorMessage();
            error.setStatus("fail");
            error.setCode("400");
            error.setMessage("InvalideRequest");
            resp.setContentType("application/json;charset=UTF-8");
                resp.getWriter().write(new Gson().toJson(error));
                throw new InvalidUserException();
        }else{
            user = (User) session.getAttribute("currentUser");
        }
        return  user;
    }

    public static User getCurrentUser(HttpServletRequest req, HttpServletResponse resp) throws IOException{
        HttpSession session = req.getSession(false);
        User user = null;
        if(session == null || session.isNew()){

        }else{
            user = (User) session.getAttribute("currentUser");
        }
        return  user;
    }
}
