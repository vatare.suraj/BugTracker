package com.bugtracker.security;
import com.bugtracker.common.GSONResponce;
import com.bugtracker.user.User;
import com.google.gson.Gson;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/LoginService")
public class LoginService extends HttpServlet{
    User user;
    LoginManager loginManager;
    Gson gson = new Gson();
    @Override
    public void init()throws ServletException{
        super.init();
        loginManager = new LoginManager();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException,IOException{
        resp.setContentType("application/json;charset=UTF-8");
        try{
            User user = ValidateSession.getCurrentUser(req, resp);
            resp.getWriter().write(gson.toJson(new GSONResponce("success", "", user)));
        }catch (Exception excepton) {
            excepton.printStackTrace();
        }
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=UTF-8");
        User user = new User();
        try{
            user.setEmail(req.getParameter("email"));
            user.setPassword(req.getParameter("password"));
            user = loginManager.add(user);
            if(user.getId() != 0){
                HttpSession session = req.getSession();
                session.setAttribute("currentUser", user);
            }
            resp.getWriter().write(gson.toJson(new GSONResponce("success", "", user)));
        }catch (Exception excepton) {
            excepton.printStackTrace();
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=UTF-8");
        User user = null;
        System.out.println("doDelete");
        try{
            HttpSession session = req.getSession(false);
            session.invalidate();
            resp.getWriter().write(gson.toJson(new GSONResponce("success", "", user)));
        }catch (Exception excepton) {
            excepton.printStackTrace();
        }
    }
}
