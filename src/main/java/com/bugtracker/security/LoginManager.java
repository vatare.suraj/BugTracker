package com.bugtracker.security;
import com.bugtracker.user.User;

public class LoginManager {
   LoginDao loginrDao;

    public LoginManager(){
        loginrDao = new LoginDao();
    }

    public User add(User user){
        user = loginrDao.add(user);
        return user;
    }
}
