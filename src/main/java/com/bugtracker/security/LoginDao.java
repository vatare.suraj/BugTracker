package com.bugtracker.security;
import com.bugtracker.common.DBConnection;
import com.bugtracker.user.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class LoginDao {
    public User add(User user) {

        try {
            System.out.println(user.getId());
            Connection con = DBConnection.getConnection();
            String query = " SELECT * FROM user where email=? and password=?";
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setString(1,user.getEmail());
            stmt.setString(2,user.getPassword());
            ResultSet resultSet = stmt.executeQuery();
            if(resultSet.next()){
                user.setId(resultSet.getInt("id"));
                user.setFirstName(resultSet.getString("firstName"));
                user.setLastName(resultSet.getString("lastName"));
                user.setEmail(resultSet.getString("email"));
            }
            else{
                user = new User();
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(user.getId());
        return user;
    }
}

