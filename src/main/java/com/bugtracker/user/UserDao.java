package com.bugtracker.user;
import com.bugtracker.role.Role;
import com.bugtracker.common.DBConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.lang.String;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UserDao {

    public void add(User user) {
        try {
            System.out.println();
            Connection con = DBConnection.getConnection();
            String query = " insert into user (firstName, lastName, email, password,role, created_date)"
                    + " values (?, ?, ?, ?, ?, ?)";
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setString(1,user.getFirstName());
            stmt.setString (2,user.getLastName());
            stmt.setString(3,user.getEmail());
            stmt.setString(4,user.getPassword());
            stmt.setString(5,user.getRole().toString());
            stmt.setTimestamp(6, new Timestamp(user.getCreated_date().getTime()));
            int i = stmt.executeUpdate();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<User> getAllUser(){
        List<User> list = new ArrayList<User>();
        try {
            Connection con = DBConnection.getConnection();
            PreparedStatement stmt = con.prepareStatement(" select * from user");
            ResultSet resultSet = stmt.executeQuery();
            while (resultSet.next())
            {

                 User user = new User();
                 user.setId(resultSet.getInt("Id"));
                 user.setFirstName(resultSet.getString("firstName"));
                 user.setLastName(resultSet.getString("lastName"));
                 user.setEmail(resultSet.getString("email"));
                 user.setCreated_date(resultSet.getDate("created_date"));

                // user.setRole(Role.valueOf(resultSet.getString("role")));
                 user.setRole(Role.TESTER);
                 user.setActive(true);
                 list.add(user);

            }

        }catch (Exception e)
        {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return list;
    }
}
