package com.bugtracker.user;

import com.bugtracker.common.GSONResponce;
import com.bugtracker.role.Role;
import com.bugtracker.security.ValidateSession;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@WebServlet("/UserService")
public class UserService extends HttpServlet {

    User user;
    UserManager userManager;
    Gson gson = new Gson();

    @Override
    public void init() throws ServletException {
        super.init();
        userManager = new UserManager();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=UTF-8");
        try {
            ValidateSession.validate(req, resp);
           // user.setId(Integer.parseInt(req.getParameter("id")));
            List<User> list = userManager.getAllUser();
            resp.getWriter().print( gson.toJson(new GSONResponce("success", "", list)) );
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=UTF-8");
        User user = new User();
        try {
            user.setFirstName(req.getParameter("firstName"));
            user.setLastName(req.getParameter("lastName"));
            user.setEmail(req.getParameter("email"));
            user.setPassword(req.getParameter("password"));
            userManager.add(user);
            resp.getWriter().print( gson.toJson(new GSONResponce("success", "", user)) );
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=UTF-8");
        try {

        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=UTF-8");
        try {

        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

}
