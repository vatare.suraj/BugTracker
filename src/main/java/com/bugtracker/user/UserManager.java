package com.bugtracker.user;

import com.bugtracker.role.Role;

import java.util.Date;
import java.util.List;

public class UserManager {

    UserDao userDao;

    public UserManager(){
        userDao = new UserDao();
    }

    public void add(User user){
        user.setCreated_date(new Date());
        user.setRole(Role.TESTER);
        userDao.add(user);
    }

    public List<User> getAllUser(){
       return userDao.getAllUser();
    }
}
