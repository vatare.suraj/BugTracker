package com.bugtracker.common;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBConnection {
    static Connection con;
    static Properties properties;

    public static Connection getConnection() {
        if(con != null){
            return  con;
        }
        else{
            try{
                con = createConnection();
            }catch (Exception e){ e.printStackTrace(); }
            return con;
        }

    }
    private  static Connection createConnection() throws ClassNotFoundException, SQLException {
        properties = getProperties();
        String driver = properties.getProperty("database.driver");
        String url = properties.getProperty("database.url");
        String userName = properties.getProperty("database.username");
        String password = properties.getProperty("database.password");
        System.out.println("creat connection");
        System.out.println(driver+" "+url+" "+userName+" "+password);

        Class.forName(driver);
        Connection con = DriverManager.getConnection(url, userName, password);
        System.out.println("Successfully");
        return con;
    }

    public static Properties getProperties() {
        if(properties != null){
            return  properties;
        }
        else{
            try{
                properties = readProperties();
            }catch (Exception e){ e.printStackTrace(); }
            return properties;
        }
    }

    private static Properties readProperties() throws IOException{
        Properties properties = new Properties();
        InputStream inputStream = DBConnection.class.getResourceAsStream("/config.properties");
        properties.load(inputStream);
        return  properties;
    }
}
