package com.bugtracker.common;

import com.google.gson.Gson;

public class GSONResponce {
    String status;
    String message;
    Object data;
    public GSONResponce(String status, String message, Object object){
        this.status = status;
        this.message = message;
        this.data = object;
    }
}
