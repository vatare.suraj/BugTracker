package com.bugtracker.project;

import com.bugtracker.common.DBConnection;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProjectDao {

    public void add(Project project) {
        try{
            Connection con = DBConnection.getConnection();
            String query = "insert into project( projectName, description, created_by, created_date)"
                    +" values(?,?,?,?)";
            PreparedStatement stmt = con.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
            System.out.println("stmt");
            stmt.setString(1,project.getName());
            stmt.setString (2,project.getDescription());
            stmt.setString(3, project.getCreatedBy());
            stmt.setTimestamp(4, new Timestamp(project.getCreatedDate().getTime()));
            stmt.executeUpdate();
            ResultSet resultSet = stmt.getGeneratedKeys();
            if (resultSet.next()) {
                int id = resultSet.getInt("GENERATED_KEY");
                project.setId(id);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public List<Project> getAllProject(){
        List<Project> list = new ArrayList<Project>();
        try {
            Connection con = DBConnection.getConnection();
            PreparedStatement stmt = con.prepareStatement(" select * from project");
            ResultSet resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                Project project = new Project();
                project.setId(resultSet.getInt("id"));
                project.setName(resultSet.getString("projectName"));
                project.setDescription(resultSet.getString("description"));
                project.setCreatedBy(resultSet.getString("created_by"));
                project.setCreatedDate(resultSet.getDate("created_date"));
                list.add(project);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return list;
    }

    public List<Project> getAllCurrentUserProject(){
        List<Project> list = new ArrayList<Project>();
        try {
            Connection con = DBConnection.getConnection();
            PreparedStatement stmt = con.prepareStatement(" select * from project");
            ResultSet resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                Project project = new Project();
                project.setId(resultSet.getInt("id"));
                project.setName(resultSet.getString("projectName"));
                project.setDescription(resultSet.getString("description"));
                list.add(project);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return list;
    }

    public void mapProjectUser(int projectId, int userId){
        try{
            Connection con = DBConnection.getConnection();
            String query = "insert into userProjectMapping( project_id, user_id)"
                    +" values(?,?)";
            PreparedStatement stmt = con.prepareStatement(query);
            System.out.println("stmt");
            stmt.setInt(1, projectId);
            stmt.setInt(2, userId);
            int i = stmt.executeUpdate();
        }catch (Exception e){ e.printStackTrace();}
    }
}
