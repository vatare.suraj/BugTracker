package com.bugtracker.project;

import com.bugtracker.common.GSONResponce;
import com.bugtracker.security.ValidateSession;
import com.bugtracker.user.User;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@WebServlet("/ProjectService")
public class ProjectService extends HttpServlet {
    ProjectManager projectManager;
    Gson gson = new Gson();

    public void init()throws ServletException{
        super.init();
        projectManager = new ProjectManager();
    }

    @Override
    protected void doGet(HttpServletRequest req , HttpServletResponse resp)throws ServletException,IOException{
        resp.setContentType("application/json;charset=UTF-8");
        try {
            User user = ValidateSession.validate(req, resp);

            List<Project> list = projectManager.getAllProject();
            resp.getWriter().print( gson.toJson(new GSONResponce("success", "", list)) );
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest reqs,HttpServletResponse resp)throws ServletException,IOException{
        resp.setContentType("application/json;charset=UTF-8");
        try{
            // check user authentication
            User user = ValidateSession.validate(reqs, resp);

            Project project = new Project();

            String usersJson = reqs.getParameter("selectedUsers");
            List<User> users = gson.fromJson(usersJson, new TypeToken<ArrayList<User>>(){}.getType());
            project.setProjectUsers(users);
            project.setName(reqs.getParameter("projectName"));
            project.setDescription(reqs.getParameter("description"));
            project.setCreatedBy(user.getEmail());
            project.setCreatedDate(new Date());
            projectManager.add(project);
            resp.getWriter().print( gson.toJson(new GSONResponce("success", "", project)) );
        }catch (Exception e){
            e.printStackTrace();
        }
    }


}
