package com.bugtracker.project;

import com.bugtracker.user.User;

import java.util.List;

public class ProjectManager {
    ProjectDao projectDao;
    public ProjectManager() {
         projectDao = new ProjectDao();
    }

    public void add(Project project){
        projectDao.add(project);
        mapProjectUser(project);
    }

    public List<Project> getAllProject(){ return projectDao.getAllProject(); }

    public List<Project> getAllCurrentUserProject(){ return projectDao.getAllProject(); }

    public void mapProjectUser(Project project){
        for(User user: project.getProjectUsers()){
            projectDao.mapProjectUser(project.getId(), user.getId());
        }
    }
}
